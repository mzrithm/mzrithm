<img src="https://hacktoberfest.digitalocean.com/_nuxt/img/logo-hacktoberfest-full.f42e3b1.svg" alt="Hacktoberfest" width ="400"/>

# Contribute to Hacktoberfest in GitLab

Hacktoberfest encourages participation in the open source communities by rewarding registrants for: 
- completing pull requests 
- participating in events
- donating to open source projects 

#### The GitLab open source community is joining the Hacktoberfest celebration this year! 🍁
This means that you will be rewarded for your contributions to participating open source repositories in GitLab.

## New to GitLab? It’s easy to sign up for free!

Start by navigating to the [GitLab sign in page](https://gitlab.com/users/sign_in). If you already have an account with one of the listed providers, you can log in to GitLab using the same user profile. Otherwise, you can register for a new account.

---

- Click `Register now` below the form.
<img src="/blankRegistration.png" alt="Blank registration" width="400"/>

---

- Fill in the form with your first name, last name, username, and password.
- Click `Register`.

<img src="/completeRegistration.png" alt="Complete registration" width="350"/>

---

- You will see a screen announcement that you have been sent an email with a confirmation link. <br>Check your email for the link to confirm your new account.

<img src="/confirmPrompt.png" alt="Confirm account prompt" width="600"/>

---

- Click the `Confirm your account` link in your email.

<img src="/confirmationLink.png" alt="Confirmation link" width="700"/>

---

### That’s it! You've created your new GitLab account.

<img src="/tanuki.png" alt="Logo" width="100"/> <br>**There are hundreds of open source projects in GitLab.** <br>Now you are one step closer to making those projects part of your open source celebration this Hacktoberfest.🍁

---

Find more guidance about how to register for this event and how to contribute to open source on the 
[Official Hacktoberfest Website](https://hacktoberfest.digitalocean.com/). <br>You can sign up anytime between October 1 and October 31, 2021.







