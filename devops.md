# GitLab is the DevOps platform, but what is DevOps?

The term DevOps is created by combining the words **Development** and **Operations**. Both Development and Operations are stages of the *Software Product Lifecycle*.

## Development

When you are focused on the Development of your software, you are building your product. At this stage, you or your engineers are collaborating on code and communicating across teams about customer needs, desired functionality, and production timelines.

## Operations

After Development, your focus shifts into Operations. You have released your software product and you are maintaining it. Collaboration across teams continues, but now you or your engineers are responding to bug reports and security risks. Your customers are reporting changing needs, collaboration across teams is producing shifting priorities, and your production timelines are based on multiple concurrent endeavors.

## DevOps Philosophy

DevOps is the philosophy in which these two stages of the Software Product Lifecycle are combined. To understand this combination, think about driving a car in circles around a race track. Traditionally, you shift into one gear for Development and you shift into another gear for Operations. This transition from one gear into another slows your progress, and it limits your abilities. For instance, it is difficult to plan, build, and release a new product if you are stuck in Operations. Those things are done in Development. But if you shift into Development, how will you maintain, grow, and market the products you have already built? Shifting gears is a manual operation. When you embrace a **DevOps Philosophy**, you are driving around the race track in a car with an automatic transition.

<img src="https://about.gitlab.com/images/press/devops-lifecycle.svg" alt="DevOps, a better way to drive your lifecycle." width="600">

## GitLab

A DevOps philosophy *optimizes the productivity associated with Development and the reliability associated with Operations*. GitLab is the DevOps platform. It provides the tools you need to embrace a DevOps lifecycle for your software product. As a single application for the entire DevOps lifecycle, **GItLab is comprehensive, seamless, secure, transparent & compliant.** It is also easy to adopt. Now that you know the value of the DevOps lifecycle, isn’t it time you [Get started with GitLab](https://about.gitlab.com/get-started/)?
